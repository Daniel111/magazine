package kh.vclipdemo.com.magazine;

import android.graphics.PointF;
import android.net.Uri;
import android.view.View;

/**
 * Created by mac on 12/5/2016 AD.
 */

public interface OnDropping {

     void dropping(Uri uri);
}
