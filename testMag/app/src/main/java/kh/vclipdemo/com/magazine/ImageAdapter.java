package kh.vclipdemo.com.magazine;
import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {

    private Cursor cursor;
    int columnIndex;
    private  OnDropping dropping;
    private Activity context;
    private GestureDetector gestureDetector;

    public ImageAdapter(Cursor cursor, int columnIndex, Activity activity){

        this.cursor = cursor;
        this.context = activity;
        this.columnIndex  = columnIndex;
        this.dropping = (OnDropping)activity;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnTouchListener {

        ImageView imageView;

        ViewHolder(final View itemView) {

            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.image_adapter);
            imageView.setOnTouchListener(this);
        }


        @Override
        public boolean onTouch(final View view, MotionEvent motionEvent) {

            image(getAdapterPosition());

            gestureDetector = new GestureDetector(new GestureDetector.SimpleOnGestureListener() {
                public void onLongPress(MotionEvent e) {
                    View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                    view.startDrag(null, shadowBuilder, view, 0);
                }
            });
            return gestureDetector.onTouchEvent(motionEvent);
        }
    }

    @Override
    public ImageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.image_list, parent, false);
        return new ImageAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        cursor.moveToPosition(position);
        int imageID = cursor.getInt(0);
        Uri uri = Uri.withAppendedPath(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, "" + imageID);
        holder.imageView.setImageURI(uri);
    }

    private void image(int position){

        cursor.moveToPosition(position);
        int imageID = cursor.getInt(0);
        Uri uri = Uri.withAppendedPath(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, "" + imageID);
        dropping.dropping(uri);
    }

    @Override
    public int getItemCount() {
        return cursor.getCount();
    }


}
