package kh.vclipdemo.com.magazine;

import android.net.Uri;
import android.view.DragEvent;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by mac on 12/5/2016 AD.
 */

public class OnDropListener implements View.OnDragListener {

    private Uri uri;

    public OnDropListener (Uri uri){
        this.uri = uri;
    }

    @Override
    public boolean onDrag(View view, DragEvent dragEvent) {

        switch (dragEvent.getAction()){

            case DragEvent.ACTION_DROP:
                ImageView imageView = (ImageView) view;
                imageView.setImageURI(uri);
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                break;
        }
        return true;
    }
}
